var host = "localhost";
var port = ":7777";     // jika default (80) maka isikan dengan empty string ""
var dir = "/fitsight/hai.php";
var url = "http://" + host + port + dir;
var cap, capfinal;

function getTanggal(){
    var d = new Date();
    var tanggal = d.getDate();
    var bulan = parseInt(d.getMonth()+1);


    if (d.getDate() < 10) {
        var tanggal = '0'+d.getDate();
    }

    if (bulan < 10) {
        var bulan = '0' + bulan;
    }
    var waktudaftar = parseInt(d.getFullYear())+'-'+bulan +'-'+ tanggal;
    return waktudaftar;
}

function getTanggalBefore(){
   	var d = new Date();
	d.setDate(d.getDate() - 1);
	var tanggal = d.getDate();
	var bulan = parseInt(d.getMonth()) + 1;
	var tahun = d.getFullYear();

	if (tanggal < 10 ) {
	    var tanggal = '0'+tanggal;
	}
	if (bulan < 10) {
	    var bulan = '0'+bulan;
	}

	var tahun = tahun +'-'+ bulan+'-'+tanggal;
    return tahun;
}

function login() {

	if ( $('#username').val() != "" && $('#password').val() != "" ) {
		var user = $('#username').val();
		var pass = $('#password').val();
		var date = getTanggal();

		$.getJSON(url,{type:"login", username:user, password:pass}).done(function(result){
			var idpengguna = result.ID.ID;
			if (result.STATUS == "YES") {
				localStorage.setItem('username',user);
				localStorage.setItem('userid', idpengguna);
				localStorage.setItem('tanggal', date);
				localStorage.setItem('capai', 0);
				localStorage.setItem('target', 0);

				window.location.href = "listbluetooth.html";
			}
			else if(result.STATUS == "NO"){
				/*alert("Username and Password don't match");
				window.location.href = "index.html";*/
				$('#myModal2').modal('show');
			}
		});
  	}
  	else{
  		$('#myModal3').modal('show');
  	}
 }

 

function getDataPencapaian(tgl, id){
	$.getJSON(url, {type:"getPencapaian", iden:id, tanggal:tgl}).done(function(result){
		console.log(result);
		var tar = result[0]["target"];
		cap = result[0]["pencapaian"];
		var capai = cap*3600;
		localStorage.setItem('capai', capai);

		var caps = localStorage.getItem('capaiperhari');

		capfinal = cap*3600;
		document.getElementById('hourTarget').innerHTML = tar;
		time = caps; //---> edit here. just this var. time in seconds
		$('.arc').circleProgress({ value: time/21600});
		setInterval(function(){ 
			caps = parseInt(caps)+1;
			localStorage.setItem('capaiperhari', caps);

			if (caps == 60) {
				var progress = localStorage.getItem('capaiperhari');
				$('.arc').circleProgress({ value: progress/21600});
			};
			/*if (caps%3600 == 0) {
				
			};*/
			//$('.arc').circleProgress({ value: time/21600, animationStartValue: });
		}, 1000);
		
		//setInterval(function(){ capfinal = capfinal+180; $('.arc').circleProgress({ value: (time/21600)+ }); }, 3000);
		//document.getElementById('hasil').innerHTML = 'target = '+ result[0]["target"] + ' dan pencapaian = ' + result[0]["pencapaian"];
		//return cap;
 	});
}

function cetakbar(res, res2){
	
	var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
        backgroundColor: 'rgba(1,1,1,0)',
		axisX:{
			interval: 1,
			labelFontSize: 10,
			lineThickness: 0
		},
		axisY2:{
			interval: 1,
			valueFormatString: "0 h",
			lineThickness: 0				
		},
		toolTip: {
			shared: true
		},
		legend:{
			verticalAlign: "top",
			horizontalAlign: "center"
		},

		data: [
		{     
			//pencapaian
			type: "stackedBar",
			showInLegend: false,
			name: "Elapsed Time",
			axisYType: "secondary",
			color: "#EBB88A",
			dataPoints: res
		},
		{     
			//sisa target
			type: "stackedBar",
			showInLegend: false,
			name: "Remain Time",
			axisYType: "secondary",
			color: "#7E8F74",
			dataPoints: res2
		}
		]
		});
	chart.render();
}


 function addPencapaian(tgl, id){
 	var id= id;
 	var tgl = tgl;
 	var target = 0;
 	var pencapaian=0;
 	
 	$.getJSON(url, {type:"addDataPencapaian", iden:id, target:"6", tanggal:tgl, pencapaian:"1"}).done(function(result){
 		alert(result.STATUS);
 		// localStorage.setItem('capai', 1);
 		// localStorage.setItem('target', 6);
 	});
 }

function updateGold(iden){
	//select
	var id = iden;

	$.getJSON(url, {type:"getGold", iden:id}).done(function(result){
 		alert(result.medal_gold.medal_gold);
 		var gold = result.medal_gold.medal_gold + 1;

 		alert('masuk update dengan id' + id);
 		//update
 		$.getJSON(url, {type:"updateGold", iden:id, golden:gold}).done(function(result){
			
		});	
 	});
	//update
 }

function updatePlatinum(iden){
	var id = iden;

	//select
	$.getJSON(url, {type:"getPlatinum", iden:id}).done(function(result){
 		//alert(result.medal_platinum.medal_platinum);
 		var plat = result.medal_platinum.medal_platinum + 1;

 		//alert('masuk update dengan id' + id);
 		//update
 		$.getJSON(url, {type:"updatePlatinum", iden:id, platinum:plat}).done(function(result){
			
		});	
 	});	

}

function updatemedal(id){
	var capai = localStorage.getItem('capai');
	var target = localStorage.getItem('target');

	var iden = id;
	if (capai < target) {
		updatePlatinum(iden);
	}
	else if(capai>target){
		updateGold(iden);
	}


}
 

 function cekDataPencapaian(){
 	var tgl = getTanggal();

 	var id = localStorage.getItem("userid");
 	//alert(tgl +' '+ id);
 	$.getJSON(url, {type:"getTanggalPencapaian", iden:id, tanggal:tgl}).done(function(result){
 		
 		var hasil = result.HITUNG;
 		if (hasil == 'YES') {
 			
 			getDataPencapaian(tgl, id);
 		}
 		else if (hasil == 'NO') {
 			var progress = localStorage.getItem('capaiperhari');
 			var capai = localStorage.getItem('capai');
 			var nilai = progress+capai;
 			var tanggalb = getTanggalBefore();
 			alert(tanggalb);
 			updatemedal(id);
 			//updatedata(id, nilai, tanggalb);
 			addPencapaian(tgl, id);
 			getDataPencapaian(tgl, id);
 			
 		};

 	});

 }

 function ambilMedal(){
 	var id = localStorage.getItem('userid');

 	$.getJSON(url, {type:"getGold", iden:id}).done(function(result){
 		//alert(result.medal_gold.medal_gold);
 		var gold = result.medal_gold.medal_gold;

 		document.getElementById('jmlgold').innerHTML = gold;
 	});

	$.getJSON(url, {type:"getPlatinum", iden:id}).done(function(result){
 		//alert('medal plat '+result.medal_platinum.medal_platinum);
 		var plat = result.medal_platinum.medal_platinum;
 		document.getElementById('jmlplatinum').innerHTML = plat;
 	});	


 }

function getDataLeaderboard(){
 	var id = localStorage.getItem("userid");
 	var tgl = getTanggal();
 	

	$.getJSON(url, {type:"getLeaderboard", iden:id}).done(function(result){
		
		var res = [];
		var res2 = [];
		var datap, datap2;
		
		 for (var i = 0; i <result.length; i++) {
			
			var hasil = {};
			hasil.y = parseInt(result[i]["pencapaian"]);
            hasil.label = result[i]["tanggal"];
            res.push(hasil);
			
		 };

		 for (var i = 0; i <result.length; i++) {
			
			var hasil2 = {};
			hasil2.y = parseInt(result[i]["target"]-result[i]["pencapaian"]);
            hasil2.label = result[i]["tanggal"];
            res2.push(hasil2);
			
		 };

		 ambilMedal();
		 cetakbar(res, res2);
 	});
}

 