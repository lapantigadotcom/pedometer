window.onload = function () {
	/*var id = sessionStorage.getItem("userid");
 	var tgl = getTanggal();*/

 	//getDataLeaderboard(id);

 	/*var da = getDataLeaderboard();
 	var satu = da[0];
 	var dua = da[1];
 	alert('satu '+satu);*/

	var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
        backgroundColor: 'rgba(1,1,1,0)',
		axisX:{
			interval: 1,
			labelFontSize: 10,
			lineThickness: 0
		},
		axisY2:{
			interval: 1,
			valueFormatString: "0 h",
			lineThickness: 0				
		},
		toolTip: {
			shared: true
		},
		legend:{
			verticalAlign: "top",
			horizontalAlign: "center"
		},

		data: [
		{     
			//pencapaian
			type: "stackedBar",
			showInLegend: false,
			name: "Elapsed Time",
			axisYType: "secondary",
			color: "#EBB88A",
			dataPoints: [
				
				{y: 3, label: "Mon 17/09"},
				{y: 1, label: "Tue 18/09" },
				{y: 3, label: "Wed 19/09"   },			
				{y: 2, label: "Thur 20/09"  },			
				{y: 4, label: "Fri 21/09"  },
				{y: 2, label: "Sat 22/09"  },
				{y: 5, label: "Sun 23/09"  }

			]
		},
		{     
			//sisa target
			type: "stackedBar",
			showInLegend: false,
			name: "Remain Time",
			axisYType: "secondary",
			color: "#7E8F74",
			dataPoints: [
				
				{axisYType: 2, label: "Mon 17/09"},
				{y: 4, label: "Tue 18/09" },
				{y: 2, label: "Wed 19/09"   },			
				{y: 3, label: "Thur 20/09"  },			
				{y: 1, label: "Fri 21/09"  },
				{y: 3, label: "Sat 22/09"  },
				{y: 0, label: "Sun 23/09"  }
			]
		}
		]
		});
	chart.render();
}