// Arc layout
var time; //in seconds.
$.circleProgress.defaults.arcCoef = 0.5; // range: 0..1
$.circleProgress.defaults.startAngle = 0.5 * Math.PI;
$.circleProgress.defaults.dashesAmount = 6;
$.circleProgress.defaults.dashSize = 0.03; // in radians

$.circleProgress.defaults.drawArc = function(v) {
    var ctx = this.ctx,
        r = this.radius,
        t = this.getThickness(),
        c = this.arcCoef,
        a = this.startAngle + (1 - c) * Math.PI;
    
    v = Math.max(0, Math.min(1, v));

    ctx.save();
    ctx.beginPath();

    if (!this.reverse) {
        ctx.arc(r, r, r - t / 2, a, a + 2 * c * Math.PI * v);
    } else {
        ctx.arc(r, r, r - t / 2, a + 2 * c * Math.PI, a + 2 * c * (1 - v) * Math.PI, a);
    }

    ctx.lineWidth = t;
    ctx.lineCap = this.lineCap;
    ctx.strokeStyle = this.arcFill;
    ctx.stroke();
    ctx.restore();
    
    this.addDashes();
};

$.circleProgress.defaults.addDashes = function() {
    var ctx = this.ctx,
        r = this.radius,
        t = this.getThickness(),
        d = this.dashSize,
        c = this.arcCoef,
        a = this.startAngle + (1 - c) * Math.PI,
        n = this.dashesAmount;
    
    ctx.save();
    ctx.lineCap = 'butt';
    ctx.lineWidth = t;
    ctx.strokeStyle = '#fff';
    
    // not very cross-browser option that makes the "dashes" transparent
    // if it will not work, remove this line - "dashes" will be white
    ctx.globalCompositeOperation = "destination-out";

    for (var i = 0; i < n; i++) {
        ctx.beginPath();
        var s = a + 2 * c * (1 - (i + 0.5) / n) * Math.PI;
        ctx.arc(r, r, r - t / 2, s - d / 2, s + d / 2);
        ctx.stroke();
    }
    
    ctx.restore();
};

$.circleProgress.defaults.drawEmptyArc = function(v) {
    var ctx = this.ctx,
        r = this.radius,
        t = this.getThickness(),
        c = this.arcCoef,
        a = this.startAngle + (1 - c) * Math.PI;

    v = Math.max(0, Math.min(1, v));
    
    if (v < 1) {
        ctx.save();
        ctx.beginPath();

        if (v <= 0) {
            ctx.arc(r, r, r - t / 2, a, a + 2 * c * Math.PI);
        } else {
            if (!this.reverse) {
                ctx.arc(r, r, r - t / 2, a + 2 * c * Math.PI * v, a + 2 * c * Math.PI);
            } else {
                ctx.arc(r, r, r - t / 2, a, a + 2 * c * (1 - v) * Math.PI);
            }
        }

        ctx.lineWidth = t;
        ctx.lineCap = this.lineCap;
        ctx.strokeStyle = this.emptyFill;
        ctx.stroke();
        ctx.restore();
    }
};

//-------------------------- Example --------------------------
$('.arc').circleProgress({
    // arc config
    arcCoef: 0.75,
    dashesAmount: 6,
    dashSize: Math.PI / 100,
    
    // default config
    size: 850,
    value: 1, //value = time / 21600
    fill: { color: '#fc0'},
    bgcolor: '#929292',
    thickness: 82,
    emptyFill: '#3c6'
}).on('circle-animation-progress', function(event, progress, stepValue) {
    $(this).find('.waktuTempuh').text((stepValue*6).toFixed(0)+'h ' + (((stepValue * 21600) % 3600)/60).toFixed(0) + ' m');
});